#include <czmq.h>
#include <string>
#include <assert.h>
#include <unordered_map>
#include <vector>

using namespace std;
zctx_t* context;
unordered_map<string, string> article; //Lista de articulos en nuestra wikipedia(hashmap)
int cant_server; // cantidad de servidores que indiquemos
vector<string> consult_servers; // lista de servidores de consulta

void Update(string title, string body){ //actualizar articulos
	void *requester_update;
	zmsg_t *msg_update;
	zframe_t *aux_update;
	char *result_update;
	
	for(int i=0; i < cant_server; i++)//For para enviar las actualizaciones a todos los servidores de consultas
		{
		requester_update =  zsocket_new(context, ZMQ_REQ);
		zsocket_connect (requester_update, consult_servers[i].c_str()); //Se conecta a determinado servidor de consulta para actualizar o agregar un articulo
	    msg_update = zmsg_new();
	    assert(msg_update);

	  	aux_update = zframe_new("update", 6);
		zmsg_add(msg_update, aux_update);
	    aux_update = zframe_new(title.c_str(), strlen(title.c_str())); //Se agrega un frame con el titulo del articulo
		zmsg_add(msg_update, aux_update);
	    aux_update = zframe_new(body.c_str(), strlen(body.c_str()));//Se agrega un frame con el cuerpo del articulo
	    zmsg_add(msg_update, aux_update);

	    zmsg_send(&msg_update, requester_update); //Se envia el mensaje al servidor para hacer la respectiva actualizacion o adicion
	    msg_update = zmsg_recv(requester_update); //Se recibe ok si la operacion fue realizada con exito
	    
	    result_update = zmsg_popstr(msg_update);

	  	if(strcmp(result_update, "ok") != 0)
			printf("%s %s\n", "Hubo un error en la actualización del servidor alojado en el puerto: ", consult_servers[i].c_str());
	
		zmsg_destroy(&msg_update);
    	zsocket_destroy(context, requester_update); 
		}
		free(result_update);
}

const char* Edit(string object, string body){ //Editar un articulo
	if(article.count(object) == 0) //Se busca el articulo en la tabla hash
	  	{ return "No existe el archivo a editar";} //Si el articulo no existe se envia este mensaje
	else
		{ unordered_map<string, string>::const_iterator got =  article.find(object); //Se itera por la tabla hash en busca del articulo
		  if (got == article.end())
			  {return "No existe el archivo a editar";} //Se envia este mensaje de error en la edicion, articulo no encontrado
		  else
	 	     {  article[object] = body; //Se edita el articulo
	 	     	Update(object, body); //Se actualizan todos los servidores de consultas
	 	        return "Archivo actualizado con exito!"; //Se retorna este mensaje del exito de la operacion		 	     	
	 	     }
		}	  
}

const char* New_Article(string title, string body){ //Agregar nuevo articulo
	if(article.count(title) == 0)//Se busca que no exista el articulo que se desea agregar
		{article[title] = body; //Se agrega el nuevo articulo a la tabla hash
		 Update(title, body); //Se agrega el nuevo articulo a todos los servidores de consultas
	 	 return "Archivo agregado con exito!";	//Mensaje de exito de la operacion
		}
	else
	 	return "Ya existe un archivo con ese nombre"; //Cuando existe un articulo con el mismo titulo
}

void dispatch(zmsg_t *msg, zmsg_t *response){
	assert(zmsg_size(msg) >= 2);
	char *funtion = zmsg_popstr(msg); //Se obtiene el tipo de solicitud
	char *title = zmsg_popstr(msg); //Se obtiene el titulo del articulo
	char *body = zmsg_popstr(msg); //Se obtiene el cuerpo del articulo
	if(strcmp(funtion, "edit") == 0) //Se identifica la solicitud para editar
		{ zmsg_addstr(response, "%s", Edit(title, body)); // Se procede a editar el articulo
		}
    else if(strcmp(funtion, "add") == 0) // Se identifica la solicitud para agregar un nuevo articulo
    	{ zmsg_addstr(response, "%s", New_Article(title, body)); //Se procede a agregar articulo
		}
    else
    	{ zmsg_addstr(response, "%s", "No se puede concretar la solicitud"); } // La solicitud que se hizo no se pudo concretar

	free(funtion);
	free(title);
	free(body);
	}

int main(int argc, char** argv){
	if(argc >= 3)
	  {	char *puerto = (char*) malloc (25);
		for(int i = 2; i < argc; i++){ //Agregamos al vector de servidores de consulta la cantidad que especifiquemos de servidores
			strcpy(puerto, "tcp://localhost:");
			strcat(puerto, argv[i]);
			string str(puerto);
			consult_servers.push_back(puerto);
		}
		cant_server = argc - 2; // Asignamos la cantidad de servidores 
		context = zctx_new();
		article["futbol"] = "El deporte mas popular del mundo";		// Agregamos dos articulos para hacer pruebas
		article["perro"] = "Mejor amigo del hombre";			 	//
		Update("futbol", "El deporte mas popular del mundo");		// Enviamos a los servidores de consultas
		Update("perro", "Mejor amigo del hombre");					//
		void *responder = zsocket_new(context, 	ZMQ_REP);
		int pn = zsocket_bind(responder, "tcp://*:%s", argv[1]);
		printf("runserver port %d\n", pn);
		while(1){
			zmsg_t *msg = zmsg_new();		
			msg = zmsg_recv(responder);
			zmsg_dump(msg);
			zmsg_t *response = zmsg_new();		
			dispatch(msg, response);
			zmsg_destroy(&msg); 		 
			zmsg_send(&response, responder);
			zmsg_destroy(&response);
		}
		zsocket_destroy(context, responder);
	  }
	  else
	  	{printf("Recuerde que deben tener minimo 2 argumentos con la siguiente estructura\nDonde (P -> Puerto, S -> Servidor)\n");
	  	printf("P-SActualizaciones P-SConsultas(1) ... P-SConsultas(n)\n");
	  	}
	return 0;
}
