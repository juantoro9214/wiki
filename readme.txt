JUAN ESTEBAN PELAEZ MARTINEZ
JUAN PABLO TORO ZAPATA

Casos de uso:

	Consultar
	Editar
	Agregar

Ejecucion: 

	1. Se debe ejecutar los serverConsult que considere necesarios, se hace de la siguiente manera:
		./serverConsult puerto_serverConsult

	2. Se ejecuta el serverUpdate, se hace de la siguiente manera:
		./serverUpdate puerto_serverUpdate [puerto_serverConsult]+

	3. Se ejecuta el broker, se hace de la siguiente manera:
		./broker puerto_broker puerto_serverUpdate [puerto_serverConsult]+

	4. Se lanzan los clientes con las correspondientes solicitudes indicando el puerto del
	   broker al cual se desean comunicar, se hace de la siguiente manera:
		./client puerto_broker solicitud

		Tipos de solicitudes y estructura

		Consultar: ./client puerto-broker consult article_title
		Editar: ./client puerto-broker edit article_title article_new_body
		Agregar: ./client puerto-broker add article_title article_body


