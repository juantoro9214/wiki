#include <czmq.h>
#include <assert.h>

char* Concat(char *s1, char *s2) //Concatenar dos cadenas 
{   char *result = (char*) malloc(strlen(s1)+strlen(s2)+2);
    strcpy(result, s1);
    strcat(result, " ");
    strcat(result, s2);   
    return result;
}

int main(int argc, char** argv){
	zctx_t *context = zctx_new();
	void *requester = zsocket_new(context, 	ZMQ_REQ);
	zsocket_connect (requester, "tcp://localhost:%s", argv[1]);//Conectamos con el broker asignado

	zmsg_t *msg = zmsg_new();
	assert(msg);
	zmsg_addstr(msg, argv[2]); //Se guarda un frame con la solicitud del cliente
	zmsg_send(&msg, requester); //Se envia al broker la solicitud del cliente
	assert(msg == NULL);
	msg = zmsg_recv(requester);	//Se recibe el puerto por el que debe hacer la peticion

	if(strcmp(argv[2], "consult") == 0){ //Se identifica la solicitud del cliente
		char *result_consult = zmsg_popstr(msg);	//Se obtiene de la pila, el puerto de comunicacion del servidor de consulta asignado
	    void *requester_consult = zsocket_new(context, ZMQ_REQ);
		zsocket_connect (requester_consult, result_consult);	//Nos conectado al servidor asignamos y hacemos la respectiva consulta
        zmsg_t *msg_consult = zmsg_new();
        assert(msg_consult);
        zframe_t *aux_consult;

        aux_consult = zframe_new(argv[2], strlen(argv[2]));//Enviamos el tipo de solicitud (consulta)
  	    zmsg_add(msg_consult, aux_consult);
	    aux_consult = zframe_new(argv[3], strlen(argv[3]));//Enviamos el titulo del articulo a consultar
	    zmsg_add(msg_consult, aux_consult);

	    zmsg_send(&msg_consult, requester_consult); //Se envia el mensaje
	    msg_consult = zmsg_recv(requester_consult);	//Se recibe la respuesta del mensaje
	    
	    result_consult = zmsg_popstr(msg_consult); //Se obtiene el cuerpo del articulo consultado
	    printf("%s\n", result_consult);//Se imprime la consulta del usuario

	    free(result_consult);
		zmsg_destroy(&msg_consult);
	    zsocket_destroy(context, requester_consult); 
	}
    else if(strcmp(argv[2], "edit") == 0 || strcmp(argv[2], "add") == 0){ //Se identifica el tipo de la solicitud del cliente 
    																	  //Agregar o editar se conecta al puerto del servidor de actualizacions
	    if(argc >= 5){ // Se verifica que se tengan los argumentos requeridos. Tienen que ser mayor o iguales que 5
	    	char *result_edit = zmsg_popstr(msg);	   
	       	void *requester_edit = zsocket_new(context, ZMQ_REQ);
			zsocket_connect (requester_edit, result_edit);//Creamos socket con la direccion del coordinador(servidor de actualizaciones)

	        zmsg_t *msg_edit = zmsg_new();
	        assert(msg_edit);
	        zframe_t *aux_edit;
	        aux_edit = zframe_new(argv[2], strlen(argv[2]));//Enviamos el tipo de solicitud agregar o editar
	  	    zmsg_add(msg_edit, aux_edit);
		    aux_edit = zframe_new(argv[3], strlen(argv[3]));//Enviamos el titulo del articulo
	  	    zmsg_add(msg_edit, aux_edit);
	  	    char *aux = (char*) malloc(strlen(argv[4])+1);
	  	    strcpy(aux, argv[4]);
			for(int i=5;i < argc; i++) //Concatenamos el cuerpo del articulo en una sola cadena
				{aux = Concat(aux, argv[i]);} //Concatanemos cada uno de los argumentos que componen el cuerpo del articulo

			aux_edit = zframe_new(aux, strlen(aux));		// Agregamos en un frame el cuerpo del articulo que queramos editar o agregar	
	  	    zmsg_add(msg_edit, aux_edit);   //Enviamos el mensaje

	  	    zmsg_send(&msg_edit, requester_edit); //Se envia el mensaje al servidor designado
	  	    msg_edit = zmsg_recv(requester_edit); //Se recibe la respuesta del mensaje
		  	
		    result_edit = zmsg_popstr(msg_edit);
		  	printf("%s\n", result_edit); //Se imprime la respuesta de la peticion

		  	free(aux);
		  	free(result_edit);
		  	zmsg_destroy(&msg_edit);
			zsocket_destroy(context, requester_edit);			
			}
		else
			{printf("\nPara hacer este peticion deben ser minimo 4 argumentos");
			 printf("\nPuerto-broker tipo-de-peticion titulo cuerpo");
			}
	}
	else	
	    printf("Petición no válida\n"); 

	zmsg_destroy(&msg);
	zsocket_destroy(context, requester);    
	return 0;
}



  
