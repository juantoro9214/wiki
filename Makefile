#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/utp/Imágenes/cs/zmq/lib/
CXX=g++ -std=c++11
ZMQ_INCLUDES=$(ZMQ)/include
ZMQ_LIBS=$(ZMQ)/lib

all: client broker serverConsult serverUpdate

client: client.o
	$(CXX) -L$(ZMQ_LIBS) -o client client.o -lzmq -lczmq

client.o: client.cc
	$(CXX) -I$(ZMQ_INCLUDES) -c client.cc

broker: broker.o
	$(CXX) -L$(ZMQ_LIBS) -o broker broker.o -lzmq -lczmq

broker.o: broker.cc
	$(CXX) -I$(ZMQ_INCLUDES) -c broker.cc

serverConsult: serverConsult.o
	$(CXX) -L$(ZMQ_LIBS) -o serverConsult serverConsult.o -lzmq -lczmq

serverConsult.o: serverConsult.cc
	$(CXX) -I$(ZMQ_INCLUDES) -c serverConsult.cc

serverUpdate: serverUpdate.o
	$(CXX) -L$(ZMQ_LIBS) -o serverUpdate serverUpdate.o -lzmq -lczmq

serverUpdate.o: serverUpdate.cc
	$(CXX) -I$(ZMQ_INCLUDES) -c serverUpdate.cc

clean:
	rm -f client client.o



