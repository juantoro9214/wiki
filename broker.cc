#include <czmq.h>
#include <string>
#include <assert.h>
#include <vector>

using namespace std;
zctx_t* context;
char *server_update;		//Servidor de actualizaciones y su respectivo puerto
int cont = 0;				//Variable que servira para distribuir la carga
int cant_server;			//Cantidad de servidores de consulta
vector<string> consult_servers; //vector que contiene la lista de servidores de consultas

void dispatch(zmsg_t *msg, zmsg_t *response){	//En esta funcion se identifica la operacion y se envian los mensajes correspondientes
	assert(zmsg_size(msg) >= 1); 
	char *request = zmsg_popstr(msg);	//Se obtiene de la pila el primer frame para identificar el tipo de solicitud
	zframe_t *aux;
	if(strcmp(request, "consult") == 0)		//Se identifica el tipo de solicitud
		{aux = zframe_new(consult_servers[cont].c_str(), 30);	//Se agrega a un frame el puerto del servidor de consulta que corresponda responder
		 if(cont == cant_server)								//una solcitud en algun momento determinado
			cont = 0; // Cuando llegue al final del vector colocamos el indice en la primera posicion del vector
		 else
		  	cont++;  //Siguiente indice en el vector
		}
	else if(strcmp(request, "edit") == 0 || strcmp(request, "add") == 0) 	//Se identifica el tipo del cliente 
		{ aux = zframe_new(server_update, 30);}		//Se envia el puerto de comunicacion del server_update
	else
		{ aux = zframe_new("Tipo de solicitud no encontrada", 40);} // Indicamos que la peticion que se hizo no es valida
	zmsg_add(response, aux);
	free(request);
	}

int main(int argc, char** argv){
	if(argc >= 4)
	  { char *puerto = (char*) malloc (25);
		for(int i = 3; i < argc; i++){ //Ingresamos al vector la lista de servidores de consulta que indiquemos
			strcpy(puerto, "tcp://localhost:");
			strcat(puerto, argv[i]);
			string str(puerto);
			consult_servers.push_back(puerto);
		}
		server_update = (char*) malloc (25);
		strcpy(server_update, "tcp://localhost:"); //asignamos el puerto en el cual esta alojado el servidor de actualizaciones
		strcat(server_update, argv[2]);

		cant_server = argc - 4; //Inicializa en 0 si la cantidad de servidores es igual a (1) si es MAYOR QUE (1) inicializa en (cantidad-1) --> (indice para el vector)
		context = zctx_new();
		void *responder = zsocket_new(context, 	ZMQ_REP);
		int pn = zsocket_bind(responder, "tcp://*:%s", argv[1]);
		printf("runserver port %d\n", pn);	
		while(1){
			zmsg_t *msg = zmsg_new();		
			msg = zmsg_recv(responder);
			zmsg_dump(msg);
			zmsg_t *response = zmsg_new();		
			dispatch(msg, response);
			zmsg_destroy(&msg); 		 
			zmsg_send(&response, responder);
			zmsg_destroy(&response);
		}
		zsocket_destroy(context, responder);
	  }
	  else
	  	{printf("\nRecuerde que deben tener minimo 3 argumentos con la siguiente estructura\nDonde (P -> Puerto, S -> Servidor) :\n");
	  	 printf("P-broker P-SActualizaciones P-SConsultas(1) ... P-SConsultas(n)\n\n");	
	  	}		
	return 0;
}
