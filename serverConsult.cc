#include <czmq.h>
#include <string>
#include <assert.h>
#include <unordered_map>

using namespace std;
zctx_t* context;
unordered_map<string, string> article; //llave:titulo_articulo , valor:cuerpo_articulo

const char* consult(string title){
	if(article.count(title) == 0) //Se verifica si el articulo existe
	  	{ return "El articulo no existe";}
	else
		{ unordered_map<string, string>::const_iterator got =  article.find(title); //Se itera por la tabla hash en busca del articulo 
		  if (got == article.end())
			 {return "El articulo no existe";}
			else
			 {	string st = got->second; //Se obtiene el articulo consultado
				return st.c_str(); //Se retorna el articulo consultado
			 }
		}
}

void dispatch(zmsg_t *msg, zmsg_t *response){ //Encargado de identificar las soliocitudes del cliente 
	assert(zmsg_size(msg) >= 1);
	char *request = zmsg_popstr(msg); //Se obtiene el tipo de solicitud
	char *title = zmsg_popstr(msg); //Se obtiene el titulo del articulo
	
	if(strcmp(request, "consult") == 0){ //Se verifica que la solicitud sea una consulta
		zmsg_addstr(response, "%s", consult(title)); //Se envia a la funcion consult el titulo y el resultado de la busqueda se almacena en response
	}
	else if(strcmp(request, "update") == 0) { //Se verifica que la solicitud sea una edicion o actualizacion
		char *body = zmsg_popstr(msg); //Se obtiene el cuerpo del articulo que se desea editar
		article[title] = body; //Se edita el articulo 
		zmsg_addstr(response, "%s", "ok"); //Se responde (ok) para confirmar que la operacion fue realizada con exito
		free(body);
	}
	else
		{zmsg_addstr(response, "%s", "Peticion no valida");} 

	free(request);
	free(title);
}

int main(int argc, char** argv){
	if(argc >= 2)
	  { char *puerto = (char*) malloc (25);	
		context = zctx_new();
		void *responder = zsocket_new(context, 	ZMQ_REP);
		int pn = zsocket_bind(responder, "tcp://*:%s", argv[1]); // Iniciamos el servidor con el puerto asignado
		printf("runserver port %d\n", pn);	
		while(1){
			zmsg_t *msg = zmsg_new();		
			msg = zmsg_recv(responder);
			zmsg_dump(msg);
			zmsg_t *response = zmsg_new();		
			dispatch(msg, response);
			zmsg_destroy(&msg); 		 
			zmsg_send(&response, responder);
			zmsg_destroy(&response);
		}
		zsocket_destroy(context, responder);
	   }
	   else
	  	printf("Recuerde que deben ingresar un puerto para este servidor\n");
	return 0;
}

